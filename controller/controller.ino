#include <VirtualWire.h>

#define transmitterPin 12
#define transmitterLedPin 13

#define leftStickBtn 2
#define rightStickBtn 3
#define leftStickX A0
#define leftStickY A1
#define rightStickX A2
#define rightStickY A3

struct Joystick {
  bool btn;
  int x;
  int y;
  int side;
};

Joystick joysticks[2];

void setCommunicationTrasmitterPin();
void setJoystickPin();

void fetchJoysticksData(Joystick joysticks[]);
Joystick getJoystickValues(String side);
void transmitJoysticksData(Joystick data[]);
void checkTransmitionState();


void setup() {
  setCommunicationTransmitterPin();
  setJoystickPin();
}

void loop() {
  fetchJoysticksData(joysticks);
  transmitJoysticksData(joysticks);
}

void setCommunicationTransmitterPin() {
  vw_set_tx_pin(transmitterPin);
  vw_set_ptt_inverted(true);
  vw_setup(2000);
  pinMode(transmitterLedPin, OUTPUT);
}

void setJoystickPin(){
  pinMode(leftStickBtn, INPUT_PULLUP);
  pinMode(rightStickBtn, INPUT_PULLUP);
  pinMode(leftStickX, INPUT);
  pinMode(leftStickY, INPUT);
  pinMode(rightStickX, INPUT);
  pinMode(rightStickY, INPUT);
}

void fetchJoysticksData(Joystick joysticks[]){
   for(int i = 0; i < 2; i++) {
    joysticks[i] = getJoystickValues(i);
  }
}

Joystick getJoystickValues(int side) {
  if(!side) {
    Joystick leftJoystick = {
      digitalRead(leftStickBtn),
      analogRead(leftStickX),
      analogRead(leftStickY),
      side
    };
    return leftJoystick;
  } else {
    Joystick rightJoystick = {
      digitalRead(rightStickBtn),
      analogRead(rightStickX),
      analogRead(rightStickY),
      side
    };
    return rightJoystick;
  }
}

void transmitJoysticksData(Joystick data[]) {
  for(int i = 0; i < 2; i++) {
    vw_send((uint8_t *)&data[i], sizeof(data[i]));
    checkTransmitionState();
    vw_wait_tx();
    delay(25);
  }
}

void checkTransmitionState(){
  if(vw_tx_active()) {
    digitalWrite(transmitterLedPin, HIGH);
  } else {
    digitalWrite(transmitterLedPin, LOW);
  }
}
