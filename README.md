# yet another drone project

## Purpose

This project is about creating a arduino based code to control a quadcopter drone.

The project current uses a MPU6050 accelerometer, a RF 443MHz emmiter and receiver,
two joysticks and an arduino nano.

## challenges
### software related
- controller -> device communication range
- controller -> device communication speed
- flight stability
- device controls (up, down, left, right, rotation)


### hardware related
- motor power
- drone design/architecture
- device weight
- battery capacity
- impact resistence