#include <VirtualWire.h>

#define receiverPin 12
#define receiverLedPin 13

struct Joystick {
  bool btn;
  int x;
  int y;
  int side;
};

Joystick rightjoystick; 
Joystick leftjoystick; 
uint8_t buf[sizeof(rightjoystick)];
uint8_t buflen = sizeof(rightjoystick);

void setCommunicationRecieverPin();
void recieveJoysticksData();
void setJoystickSide(int side);
void printRecievedData();

void setup() {
  setCommunicationRecieverPin();
  Serial.begin(9600);
}

void loop() {
    receiveJoysticksData();
}

void setCommunicationRecieverPin() {
    pinMode(receiverLedPin, OUTPUT);
    vw_set_rx_pin(receiverPin);
    vw_setup(2000);
    vw_rx_start(); 
}

void receiveJoysticksData() {
    if (vw_get_message(buf, &buflen)) {
      digitalWrite(receiverLedPin, HIGH);
      setJoystickSide(buf.side);
      printReceivedData();
      digitalWrite(led_pin, LOW);
    }
}

void setJoystickSide(int side) {
    if(!side) {
        memcpy(&leftjoystick, &buf, buflen);
    } else {
        memcpy(&rightjoystick, &buf, buflen);
    }
}

void printReceivedData() {
    Serial.print("Got: ");
	
	for (int i = 0; i < buflen; i++) {
	    Serial.print(buf[i], HEX);
	    Serial.print(' ');
	}

	Serial.println();
}
