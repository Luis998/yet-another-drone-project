#include <MPU6050_tockn.h>
#include <Wire.h>

// EXAMPLE FOUND IN http://robojax.com/learn/arduino/?vid=robojax-mpu6050-basic

MPU6050 mpu6050(Wire);
struct Accelerometer {
    float angleX;
    float angleY;
    float angleZ;
};

const int ACCELEROMETER_MEASURE_CYCLE = 5;
int accelerometerMeasureCounter = 0;
Accelerometer accelerometerMeasures[ACCELEROMETER_MEASURE_CYCLE];

long timer = 0;

void setAccelerometer();
void setAccelerometerAngles();
void getAccelerometerAngles();
Accelerometer getAccelerometerAnglesSum();
void getAccelerometerAnglesAverage(Accelerometer accelerometerSum);

void setup() {
  Serial.begin(9600);
  setAccelerometer();
}

void loop() {
    Accelerometer accelerometerAverage;
    setAccelerometerAngles();
}

void setAccelerometer() {
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
}

void setAccelerometerAngles() {
    if (accelerometerMeasureCounter == ACCELEROMETER_MEASURE_CYCLE) {
        accelerometerMeasureCounter = 0;
        Accelerometer accelerometerSum = getAccelerometerAnglesSum();
        getAccelerometerAnglesAverage(accelerometerSum);
    } else {
        getAccelerometerAngles();
    }
}

Accelerometer getAccelerometerAnglesSum() {
    Accelerometer accelerometerSum;

    for (int i = 0; i < ACCELEROMETER_MEASURE_CYCLE; i++) {
        accelerometerSum.angleX += accelerometerMeasures[i].angleX;
        accelerometerSum.angleY += accelerometerMeasures[i].angleY;
        accelerometerSum.angleZ += accelerometerMeasures[i].angleZ;
    }

    return accelerometerSum;
}

void getAccelerometerAnglesAverage(Accelerometer accelerometerSum) {
    accelerometerAverage.angleX = accelerometerSum.angleX / ACCELEROMETER_MEASURE_CYCLE;
    accelerometerAverage.angleY = accelerometerSum.angleY / ACCELEROMETER_MEASURE_CYCLE;
    accelerometerAverage.angleZ = accelerometerSum.angleZ / ACCELEROMETER_MEASURE_CYCLE;
}

void getAccelerometerAngles() {
    mpu6050.update();

    if (millis() - timer > 100) {
        int MAX_ANGLE = 15;
        float angleX = mpu6050.getAngleX();
        float angleY = mpu6050.getAngleY();
        float angleZ = mpu6050.getAngleZ();
        Accelerometer accelerometerMeasure = {
            (angleX < MAX_ANGLE) ? angleX : MAX_ANGLE,
            (angleY < MAX_ANGLE) ? angleY : MAX_ANGLE,
            angleZ
        };
        accelerometerMeasures[accelerometerMeasureCounter] = accelerometerMeasure;
        accelerometerMeasureCounter++;  
        timer = millis();  
    } 
}