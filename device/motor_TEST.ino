#define motor1 6
#define motor2 9
#define motor3 10
#define motor4 11

int motors[] = { motor1, motor2, motor3, motor4 };
int motorSpeeds[4] = { 0 };


/*******************************************************
** The below code is get and set on the device.ino code, 
receiving the data from the controller. It is here just
to simplification of the logic and must be removed once
the parts of the project were put together.
********************************************************
struct Joystick {
  bool btn;
  int x;
  int y;
  int side;
};

Joystick rightjoystick; 
Joystick leftjoystick; 

********************************************************
********************************************************
*********************************************************/
void setMotorPin();
void controlMotorsSpeed();
void power();
void turnDirection(int motor1, int motor2);
void leanDirection(int motor1, int motor2);
void cleanMotorSpeed();


void setup() {
    setMotorPin();
}

void loop() {
    controlMotorsSpeed();
}

void setMotorPin() {
    for (int i = 0; i < 4; i++) {
        pinMode(motors[i], OUTPUT);
    }
}

void controlMotorsSpeed() {

    if (leftJoystick.y < 500) {
        turnDirection(0, 3);
    } else if (leftJoystick.y > 530) {
        turnDirection(1, 2);
    } else {
        power();
    }
    
    if (rightJoystick.x < 500) {
        leanDirection(0, 2);
    } else if (rightJoystick.x > 530) {
        leanDirection(1, 3);
    } else {
        power();
    }
    
    if(rightJoystick.y < 500) {
        leanDirection(0, 1);
    } else if (rightJoystick.y > 530) {
        leanDirection(2, 3);
    } else {
        power();
    }

    cleanMotorSpeed();
}

void power() {
    int motorSpeed = map(leftjoystick.x, 0, 1023, 0, 255);
    for (int i = 0; i < 4; i++) {
        if(motorSpeed < motorSpeeds[i]) {
            analogWrite(motors[i], motorSpeed);
        }
    }
}

void turnDirection(int motor1, int motor2) {
    const int BASE_JOYSTICK_VALUE = 512;
    int joystickDifference = abs(leftJoystick.y - BASE_JOYSTICK_VALUE);
    int reducedMotorSpeed = map(leftJoystick.x - (joystickDifference / 5), 0, 1023, 0, 255);
    int totalMotorSpeed = map(leftJoystick.x, 0, 1023, 0, 255);

    for (int i = 0; i < 4; i++) {
        if (i == motor1 || i == motor2) {
            analogWrite(motors[i], reducedMotorSpeed);
            motorSpeed[i] = reducedMotorSpeed;  
        } else {
            analogWrite(motors[i], totalMotorSpeed); 
            motorSpeed[i] = totalMotorSpeed;   
        }
    }
}

void leanDirection(int motor1, int motor2) {
    const int BASE_JOYSTICK_VALUE = 512;
    int joystickDifference = abs(rightJoystick.y - BASE_JOYSTICK_VALUE);
    int reducedMotorSpeed = map(rightJoystick.x - (joystickDifference / 5), 0, 1023, 0, 255);
    int totalMotorSpeed = map(leftJoystick.x, 0, 1023, 0, 255);

    for (int i = 0; i < 4; i++) {
        if ((i == motor1 && reducedMotorSpeed < motorSpeed[i]) || (i == motor2 && reducedMotorSpeed < motorSpeed[i])) {
            analogWrite(motors[i], reducedMotorSpeed);
            motorSpeed[i] = reducedMotorSpeed;  
        } else if (totalMotorSpeed < motorSpeed[i]) {
            analogWrite(motors[i], totalMotorSpeed);
            motorSpeed[i] = totalMotorSpeed;     
        }
    }
}

void cleanMotorSpeed() {
    for (int i = 0; i < 4; i++) {
        motorSpeed[i] = 0;
    }
}